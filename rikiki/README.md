Rikiki is a small Wiki engine in python3 I built for [Shortest Wiki Contest](http://wiki.c2.com/?ShortestWikiContest).

Rikiki means "tiny" in french.

Fun fact: Rikiki source code is *three times smaller* than this readme file.

# History

* First version : 532 bytes
* Second version : 507 bytes
* Third version : 477 bytes
* Fourth version : 469 bytes


# Usage

**YOU SHALL NOT USE THIS CODE IN PRODUCTION!**

This code is **NOT SECURE** and it is based onto CGI technology which is **OBSOLETE** for production.

To run this wiki, you need python3 only :

1. Create a folder named "wiki"
2. Create a sub-folder named "cgi-bin"
3. Copy rikiki.py inside the "cgi-bin" folder
4. chmod +x on rikiki.py
5. Run this command from the wiki folder :
    PYTHONWARNINGS="ignore" python -m http.server --cgi

Now, you can go and visit : http://localhost:8000/cgi-bin/rikiki.py?p=HomePage


# Features

As stated in the Shortest Wiki Contest consraints,there are some [Wiki Principles](http://wiki.c2.com/?WikiPrinciples)
to obey :

* Automatic Link Generation: Each page is in CamelCase. As soon as you write it in source, it is available via a link.
* Content Editable By All: Totally open, **everyone** can change sources and create more pages.
* EasyTextInput: Paragraphs are done with breaking lines, link to other pages are done in CamelCase.
* Back Links: If a page A links to a page B, this page B will display a link to page A.


## Constraints

In order to gain bytes, you can't do a lot :

* Only ASCII characters are managed
* Formating text is *highly* limited ;o)
* Input textarea should be resized before typing
* No versioning of sources
