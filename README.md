# Source codes

"International Obfuscated Python Code Competition" and other codes...

## palindrome

_yp.py_ is a source code which has the property of outputs its own source code reversed
which you can execute again to obtain the same code.


## iopcc 2023

My submission to iopcc 2023. This source code compute fibonacci numbers by giving script
the range of the month.

Plus, source code has been tweaked to be seen as a rabbit in ASCII art.


## rikiki

Smallest Wiki in python3 ? Maybe, but I got more work to do on it.
