# Remarks

This source code returns the fibonacci number at the end of the nth month as explained
on wikipedia : https://en.wikipedia.org/wiki/Fibonacci_sequence

When used without any argument, it works like month 0.

As stated in the source code: "Rabbits are great to compute fibonnacci numbers"

## Author

Mindiell (iopcc@mindiell.net)

### Help and acknowledgments

Thanks to AFPy and especially to mdk and entwanne for their help.
Thanks to Row for its original ASCII art of a delicious rabbit.

## Usage

    python fibonacci.py N

Where N is the month. It returns corresponding fibonacci number.

## Compatibility

No external library is necessary to run it.

### Python versions

It should work on python 3.10 and 3.11 only.

### OS

It has been succesfully launched on Debian and WSL Debian.

